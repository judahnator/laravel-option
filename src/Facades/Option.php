<?php

namespace judahnator\LaravelOption\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Option
 *
 * @method static void delete(string $key)
 * @method static mixed get(string $key, $default = null)
 * @method static bool has(string $key)
 * @method static void set(string $key, $value)
 *
 * @package judahnator\LaravelOption\Facades
 */
class Option extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \judahnator\Option\Option::class;
    }
}
