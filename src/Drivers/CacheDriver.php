<?php

namespace judahnator\LaravelOption\Drivers;

use Illuminate\Support\Facades\Cache;
use judahnator\Option\OptionInterface;

class CacheDriver implements OptionInterface
{
    public static function cacheKey(string $key): string
    {
        return config('options.prefix').sha1($key);
    }

    /**
     * Removes an option from the option keystore.
     *
     * @param string $key
     */
    public function delete(string $key): void
    {
        if ($this->has($key)) {
            Cache::forget(self::cacheKey($key));
        }
    }

    /**
     * Retrieve the option with a given key, or the $default if the option cannot be found.
     *
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return Cache::get(self::cacheKey($key), $default);
    }

    /**
     * Determine if the option keystore has a given $key.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return Cache::has(self::cacheKey($key));
    }

    /**
     * Set a given option to a provided $value, overwriting existing data if necessary.
     *
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value): void
    {
        Cache::forever(self::cacheKey($key), $value);
    }
}
