<?php

namespace judahnator\LaravelOption\Drivers;

use Illuminate\Support\Facades\DB;
use judahnator\Option\OptionInterface;

class DatabaseDriver implements OptionInterface
{

    /**
     * Removes an option from the option keystore.
     *
     * @param string $key
     */
    public function delete(string $key): void
    {
        if ($this->has($key)) {
            DB::table('options')
                ->where('key', $key)
                ->delete();
        }
    }

    /**
     * Retrieve the option with a given key, or the $default if the option cannot be found.
     *
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        if (!$this->has($key)) {
            return $default;
        }
        /** @var \stdClass $row */
        $row = DB::table('options')->where('key', $key)->first();
        try {
            return \unserialize($row->value);
        } catch (\ErrorException $e) {
            return $row->value;
        }
    }

    /**
     * Determine if the option keystore has a given $key.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return DB::table('options')
            ->where('key', $key)
            ->exists();
    }

    /**
     * Set a given option to a provided $value, overwriting existing data if necessary.
     *
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value): void
    {
        if (!is_string($value)) {
            $value = serialize($value);
        }
        if ($this->has($key)) {
            DB::table('options')
                ->where('key', $key)
                ->update([
                    'value' => $value
                ]);
        } else {
            DB::table('options')
                ->insert([
                    'key' => $key,
                    'value' => $value
                ]);
        }
    }
}
