<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->string('key')->unique();
            $table->longText('value');

            $table->primary('key');
            $table->index('key');
        });
    }

    public function down()
    {
        Schema::dropIfExists('options');
    }
}
