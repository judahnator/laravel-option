<?php

use judahnator\LaravelOption\Facades\Option;

if (!function_exists('delete_option')) {
    function delete_option(string $key): void
    {
        Option::delete($key);
    }
}

if (!function_exists('get_option')) {
    function get_option(string $key, $default = null)
    {
        return Option::get($key, $default);
    }
}

if (!function_exists('has_option')) {
    function has_option(string $key): bool
    {
        return Option::has($key);
    }
}

if (!function_exists('set_option')) {
    function set_option(string $key, $value): void
    {
        Option::set($key, $value);
    }
}
