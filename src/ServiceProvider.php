<?php

namespace judahnator\LaravelOption;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use judahnator\LaravelOption\Commands as Commands;
use judahnator\LaravelOption\Drivers\CacheDriver;
use judahnator\LaravelOption\Drivers\DatabaseDriver;
use judahnator\LaravelOption\Drivers\JsonFileDriver;
use judahnator\LaravelOption\Drivers\MemoryDriver;
use judahnator\Option\Option;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path('options.php'),
        ], 'config');

        $timestamp = date('Y_m_d_His', time());

        $this->publishes([
            __DIR__ . '/Migrations/0000_00_00_000000_create_options_table.php' => database_path("/migrations/{$timestamp}_create_options_table.php")
        ], 'migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\Delete::class,
                Commands\Has::class,
                Commands\Get::class,
                Commands\Set::class
            ]);
        }
    }

    public function register(): void
    {
        Application::getInstance()->bind(Option::class, function () {
            switch (config('options.driver')) {

                case 'cache':
                    $driver = new CacheDriver();
                    break;

                case 'database':
                    $driver = new DatabaseDriver();
                    break;

                case 'json':
                    $driver = new JsonFileDriver(config('options.options_file'));
                    break;

                case 'memory':
                    $driver = new MemoryDriver();
                    break;

                default:
                    $className = config('options.driver');
                    if (!$className) {
                        throw new \LogicException('No driver specified');
                    } elseif (class_exists($className)) {
                        $driver = new $className;
                    } else {
                        throw new \InvalidArgumentException("The {$className} driver is invalid.");
                    }
                    break;

            }
            return new Option($driver);
        });
    }
}
