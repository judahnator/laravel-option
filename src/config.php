<?php

return [

    /*
     * The driver for the Option class to use.
     *
     * Available options are:
     * - "cache"    - Relies on the Laravel caching subsystem.
     * - "database" - Uses the "options" table (you need to run the migration) as the store.
     * - "json"     - Uses a JSON file (location defined below) for flat-file storage.
     * - "memory"   - Useful for debugging, less useful in production.
     *
     * You may also specify the fully qualified name of a custom driver.
     * eg: 'driver' => \path\to\my\driver::class
     *
     * Your driver MUST implement the \judahnator\Option\OptionInterface interface to work.
     * The default MemoryDriver might be a good base to extend if you don't know where to start.
     */
    'driver' => env('OPTION_DRIVER', 'memory'),

    /*
     * The prefix for the Cache driver.
     * This has no effect on the other drivers.
     */
    'prefix' => env('OPTION_PREFIX', 'option_'),

    /*
     * If the JSON driver is being used, this defines where the options.json file lives.
     */
    'options_file' => env('OPTION_FILE', storage_path('options.json'))

];
