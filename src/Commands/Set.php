<?php

namespace judahnator\LaravelOption\Commands;

use Illuminate\Console\Command;
use judahnator\LaravelOption\Facades\Option;

final class Set extends Command
{
    protected $signature = 'option:set {option} {value}';

    protected $description = 'Sets a given option to a value';

    public function handle(): void
    {
        /** @var String $option */
        $option = $this->argument('option');
        Option::set($option, $this->argument('value'));
        $this->line('OK!');
    }
}
