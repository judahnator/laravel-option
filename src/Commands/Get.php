<?php

namespace judahnator\LaravelOption\Commands;

use Illuminate\Console\Command;
use judahnator\LaravelOption\Facades\Option;

final class Get extends Command
{
    protected $signature = 'option:get {option}';

    protected $description = 'Sets an option';

    public function handle(): void
    {
        /** @var string $optionName */
        $optionName = $this->argument('option');
        if (!Option::has($optionName)) {
            $this->error('Option not found');
            return;
        }
        $option = Option::get($optionName);
        if (!is_string($option)) {
            $this->output->writeln((string)print_r($option, true));
            return;
        }
        $this->line(Option::get($optionName));
    }
}
