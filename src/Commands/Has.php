<?php

namespace judahnator\LaravelOption\Commands;

use Illuminate\Console\Command;
use judahnator\LaravelOption\Facades\Option;

final class Has extends Command
{
    protected $signature = 'option:has {option}';

    protected $description = 'Checks to see if the application has a given option';

    public function handle(): void
    {
        /** @var string $option */
        $option = $this->argument('option');
        $this->line(Option::has($option) ? 'Yes' : 'No');
    }
}
