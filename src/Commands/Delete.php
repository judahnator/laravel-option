<?php

namespace judahnator\LaravelOption\Commands;

use Illuminate\Console\Command;
use judahnator\LaravelOption\Facades\Option;

final class Delete extends Command
{
    protected $signature = 'option:delete {option}';

    protected $description = 'Removes a given option.';

    public function handle(): void
    {
        /** @var string $option */
        $option = $this->argument('option');
        if (!Option::has($option)) {
            $this->error('The given option is not set!');
            return;
        }
        Option::delete($option);
        $this->line('Option deleted');
    }
}
