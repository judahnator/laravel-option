<?php

namespace judahnator\LaravelOption\Tests;

use judahnator\LaravelOption\Facades\Option;

class ConsoleCommandsTest extends TestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return 'memory';
    }

    public function testDeleteCommand(): void
    {
        $this
            ->artisan('option:delete', ['option' => 'foo'])
            ->expectsOutput('The given option is not set!');

        Option::set('foo', 'bar');

        $this
            ->artisan('option:delete', ['option' => 'foo'])
            ->expectsOutput('Option deleted');

        $this->assertFalse(Option::has('foo'));
    }

    public function testHasCommand(): void
    {
        $this
            ->artisan('option:has', ['option' => 'foo'])
            ->expectsOutput('No');

        Option::set('foo', 'bar');

        $this
            ->artisan('option:has', ['option' => 'foo'])
            ->expectsOutput('Yes');
    }

    public function testGetCommand(): void
    {
        $this
            ->artisan('option:get', ['option' => 'foo'])
            ->expectsOutput('Option not found');

        Option::set('foo', 'bar');

        $this
            ->artisan('option:get', ['option' => 'foo'])
            ->expectsOutput('bar');

        Option::set('foo', ['bar', 'baz']);

        $this
            ->artisan('option:get', ['option' => 'foo'])
            ->expectsOutput(print_r(['bar', 'baz'], true));
    }

    public function testSetCommand(): void
    {
        $this->assertFalse(Option::has('foo'));

        $this
            ->artisan('option:set', ['option' => 'foo', 'value' => 'bar'])
            ->expectsOutput('OK!');

        $this->assertTrue(Option::has('foo'));
        $this->assertEquals('bar', Option::get('foo'));
    }
}
