<?php

namespace judahnator\LaravelOption\Tests;

/**
 * Class FunctionsTest
 *
 * This class tests the functions rather than the Option class itself.
 *
 * @package judahnator\LaravelOption\Tests
 */
class FunctionsTest extends TestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return 'memory';
    }

    public function testDeleteFunction(): void
    {
        set_option('foo', 'bar');
        delete_option('foo');
        $this->assertNull(get_option('foo'));
    }

    public function testGetFunction(): void
    {
        $this->assertNull(get_option('foo'));
        $this->assertEquals('foobar', get_option('foo', 'foobar'));
    }

    public function testHasFunction(): void
    {
        $this->assertFalse(has_option('hasTest'));
        set_option('hasTest', 'foo');
        $this->assertTrue(has_option('hasTest'));
    }

    public function testSetFunction(): void
    {
        set_option('testSet', 'foo');
        $this->assertEquals('foo', get_option('testSet'));
    }
}
