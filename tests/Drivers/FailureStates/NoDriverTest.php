<?php

namespace judahnator\LaravelOption\Tests\Drivers\FailureStates;

class NoDriverTest extends FailureTestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return '';
    }

    /**
     * Handles the "expects exception" logic for all the test cases.
     */
    public function failureToExpect(): void
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('No driver specified');
    }
}
