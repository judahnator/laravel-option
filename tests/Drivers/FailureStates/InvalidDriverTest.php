<?php

namespace judahnator\LaravelOption\Tests\Drivers\FailureStates;

class InvalidDriverTest extends FailureTestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return "class_not_exists";
    }

    /**
     * Handles the "expects exception" logic for all the test cases.
     */
    public function failureToExpect(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The class_not_exists driver is invalid.');
    }
}
