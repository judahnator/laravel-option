<?php

namespace judahnator\LaravelOption\Tests\Drivers\FailureStates;

use judahnator\LaravelOption\Tests\Drivers\DriverTestCase;

abstract class FailureTestCase extends DriverTestCase
{

    /**
     * Handles the "expects exception" logic for all the test cases.
     */
    abstract public function failureToExpect(): void;

    public function testDeleteMethod(): void
    {
        $this->failureToExpect();
        parent::testDeleteMethod();
    }

    public function testGetMethod(): void
    {
        $this->failureToExpect();
        parent::testGetMethod();
    }

    public function testHasMethod(): void
    {
        $this->failureToExpect();
        parent::testHasMethod();
    }

    public function testSetMethod(): void
    {
        $this->failureToExpect();
        parent::testSetMethod();
    }
}
