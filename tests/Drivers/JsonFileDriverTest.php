<?php

namespace judahnator\LaravelOption\Tests\Drivers;

use Illuminate\Support\Facades\File;

class JsonFileDriverTest extends DriverTestCase
{

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getApplicationAliases($app)
    {
        return array_merge(
            parent::getApplicationAliases($app),
            [
                'File' => File::class
            ]
        );
    }

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return 'json';
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('options.options_file', __DIR__.'/../options.json');
    }

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        file_put_contents(__DIR__.'/../options.json', '{}');
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    protected function tearDown()
    {
        parent::tearDown();
        if (file_exists(__DIR__.'/../options.json')) {
            unlink(__DIR__.'/../options.json');
        }
    }
}
