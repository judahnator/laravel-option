<?php

namespace judahnator\LaravelOption\Tests\Drivers;

use judahnator\LaravelOption\Drivers\MemoryDriver;
use judahnator\Option\OptionInterface;

class CustomDriverTest extends DriverTestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return get_class(
            new class extends MemoryDriver implements OptionInterface {
            }
        );
    }
}
