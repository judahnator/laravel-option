<?php

namespace judahnator\LaravelOption\Tests\Drivers;

class MemoryDriverTest extends DriverTestCase
{
    public function getConfigurationDriver(): string
    {
        return 'memory';
    }
}
