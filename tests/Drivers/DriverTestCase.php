<?php

namespace judahnator\LaravelOption\Tests\Drivers;

use judahnator\LaravelOption\Facades\Option;
use judahnator\LaravelOption\Tests\TestCase;

abstract class DriverTestCase extends TestCase
{
    public function testDeleteMethod(): void
    {
        Option::set('foo', 'bar');
        Option::delete('foo');
        $this->assertFalse(Option::has('foo'));
    }

    public function testGetMethod(): void
    {
        // Make sure null is returned if no default is set
        $this->assertNull(Option::get('foo'));

        // Make sure the default value is returned
        $this->assertEquals('bar', Option::get('foo', 'bar'));
    }

    public function testHasMethod(): void
    {
        Option::set('foo', 'bar');
        $this->assertTrue(Option::has('foo'));
    }

    public function testSetMethod(): void
    {
        Option::set('string', 'foobar');
        Option::set('array', ['foo', 'bar']);
        Option::set('object', (object)['foo' => 'bar']);

        $this->assertEquals('foobar', Option::get('string'));

        $this->assertEquals('foo', Option::get('array')[0]);
        $this->assertEquals('bar', Option::get('array')[1]);

        $this->assertEquals('bar', Option::get('object')->foo);
    }
}
