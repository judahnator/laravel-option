<?php

namespace judahnator\LaravelOption\Tests\Drivers;

use Illuminate\Support\Facades\Cache;

class CacheDriverTest extends DriverTestCase
{

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    public function getConfigurationDriver(): string
    {
        return 'cache';
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return array_merge(
            parent::getPackageProviders($app),
            [
                'Cache' => Cache::class
            ]
        );
    }
}
