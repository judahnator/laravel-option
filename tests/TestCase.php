<?php

namespace judahnator\LaravelOption\Tests;

use judahnator\LaravelOption\Facades\Option as OptionFacade;
use judahnator\LaravelOption\ServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('options.driver', $this->getConfigurationDriver());
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getApplicationAliases($app)
    {
        return [
            'Option' => OptionFacade::class
        ];
    }

    /**
     * Returns the config option for the driver to use.
     *
     * @return string
     */
    abstract public function getConfigurationDriver(): string;

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }
}
