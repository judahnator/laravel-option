Laravel Options Package
=======================
[![pipeline status](https://gitlab.com/judahnator/laravel-option/badges/master/pipeline.svg)](https://gitlab.com/judahnator/laravel-option/commits/master)

This is a simple package to provide a key=>value option store for Laravel.

This package was inspired by the WordPress `get_option()` functionality.
If you are interested in building a non-laravel implementation, check
out my `judahnator/option` package.
A simple key/value store to keep basic site settings persistent, especially
settings that change too frequently to add to your env but that do not 
justify having a whole application model for.

Installation
------------

Installation is fairly straightforward. Simply require this package via
composer.

`composer require judahnator/laravel-option`

You will also need to publish some assets. Run the `php artisan vendor:publish --tag=config`
command and follow the instructions there to publish.

Last you will need to configure how you want to store your options. Open up the new `config/options.php` file that was created and look through the options there. Here is a basic overview on how each of the driver options will work.

---

**cache:** 

Uses the Laravel Caching subsystem. Your options will be persistant until you clear your application cache.

**database:** 

Uses a database table to store configuration values. You will need to publish and run a migration in order to use this.
Run `php artisan vendor:publish --tag=migrations` then `php artisan migrate`

**json:**

Stores all the options in a flat JSON file. In order to use this driver you will need to create an options file and set the "options_file" setting in the same file to its location.

It is highly recomended to keep your options file out of version control. Perhaps have an 'options.json.example' file as a template, keeping your 'options.json' file in your gitignore file.

**memory:**

A 100% in-memory driver. Options will not persist through a reload. This is mostly handy for a proof of concept or for debugging.

Usage
-----

Usage is super straightforward.

* To get an option:
  * `\Option::get('foo', 'optional default value if foo not found')`
* To check if an option exists:
  * `\Option::has('foo')`
* To set (or overwrite) an option:
  * `\Option::set('foo', 'foos value')`
* To delete an option:
  * `\Option::delete('foo')`
  
For convenience sake I have also added some helper functions. They 
are just aliases of the above, but you are free to use them as well.

* `delete_option($key)`
* `get_option($key, $default)`
* `has_option($key)`
* `set_option($key, $value)`

You may also interact with the library via artisan commands, which should help with automation and other CLI endpoints.
Here are some examples of how that might work:
```bash
# Deletes the option if it exists, throws an error if not
php artisan option:delete option_to_delete;

# Prints 'Yes' or 'No'
php artisan option:has my_option;

# Prints the value of the option.
# If the option is not a string the value will be "print_r"'d
php artisan option:get super_option

# Sets the value of an option
php artisan option:set some_option some_value
```

Custom Drivers
--------------

If you do not like any of the default drivers and want to use your own,
no problem! The method of doing so is fairly straightforward.

Create your "driver," which is a class that implements the 
`\judahnator\Option\OptionInterface` interface. That interface
defines all of the functions required. If you are looking for a starting
point, you can extend the `\judahnator\LaravelOption\Drivers\MemoryDriver`
class. Check out the `\judahnator\LaravelOption\Drivers\JsonFileDriver`
for an example of how to do just that.

Once you have your driver written, you need to add it to your config
file. For the "driver" option add the full path to your class.
For example:

```php
[
    // ...
    
    'driver' => \Path\To\Custom\Driver::class,
    
    // ...
]
```